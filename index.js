class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}

	login(){

        console.log(`${this.email} has logged in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {

       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    }
}


//class Section will allow us to group our students as a section.

class Section {

    //every instance of Section class will be instantiated with an empty array for our students.
    constructor(name) {
        this.name = name;
        this.students = [];
        //add a counter or number of honor students:
        this.honorStudents = undefined;
        this.average = undefined;


    }
    //addStudents method will allow us to add instances of the Student class items for our students property.
    addStudent(name,email,grades){
        
        //A Student instance/object will be instantiated with the name, email, grades and pushed into our students property.

        this.students.push(new Student(name,email,grades));

        return this;

    }

    //countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors
    countHonorStudents(){
        let count = 0;
        //loop over each Student instance in the students array
        this.students.forEach(student => {

            //log the student instance currently being iterated.
            //console.log(student); 

            //log each student student's isPassedWithHonors property:
            
            //invoke will pass with honors so we can determine and add wheter the student passed with honors in its property.

            student.willPassWithHonors();
            //isPassedWithHonors property of the student should be populated
            //console.log(student)
            
            //Check if student isPassedWithHonor
            //add 1 to a temporary variable to hold the number of honorStudents
            
            if(student.isPassedWithHonors){
                count++
            }
        })

        //update the honorStudents property with the update value of count:
        this.honorStudents = count;

        return this;
    };
    getNumberOfStudents(){
        let count = 0;
  
        this.students.forEach(student => {
     
            if(student.name){
                count++
            }
        })
        this.students = count;

        return this;
    };
    countPassedStudents(){
        let count = 0;
       
        this.students.forEach(student => {

            student.computeAve().willPass();
           
            if(student.isPassed){
                count++
            }
        })
        this.students = count;

        return this;
    };
    computeSectionAve(){
        let count = 0;
       
        this.students.forEach(student =>{
        let avg = student.computeAve().average
        let sum = avg + count
        count = sum
        console.log(sum)
        })
        
        this.sectionAve = Math.round(count/5);

        return this;
    };
}
let section1A = new Section("Section1A");

console.log(section1A);

//3 arguments that are needed to instantiate our student

section1A.addStudent("Joy","joy@mail.com", [89,91,92,88]);
section1A.addStudent("Jeff","jeff@mail.com", [81,80,82,78]);
section1A.addStudent("John","john@mail.com", [91,90,92,96]);
section1A.addStudent("Jack","jack@mail.com", [95,92,92,93]);
section1A.addStudent("Alex","alex@mail.com", [84,85,85,86]);
//console.log(section1A);

//check the details of John from section1A?
//console.log(section1A.students[2]);

//check the average of John from section1A?
//console.log("John's average:",section1A.students[2].computeAve().average);

//check if Jeff from section1A passed?
//console.log("Did Jeff pass?",section1A.students[1].willPass().isPassed);

//display the number of honor students in our section.
section1A.countHonorStudents();

//check the number of students:
console.log(section1A.countHonorStudents().honorStudents);
  

//Function Coding

//1. Add a new student in section1A wuth the following details:
    /* 
      a. Name:"Alex"
      b. Email:"alex@mail.com"
      c. Grades: [84,85,85,86]  
    */

//Answer:section1A.addStudent("Alex","alex@mail.com", [84,85,85,86]);

//2. Add a new class method to class Section called getNumberOfStudents
    /* 
        a. This method will simply return the number of students in the section1A. 
        b. This method is unchainable.
    */
/*Answer:  getNumberOfStudents(){
        let count = 0;
  
        this.students.forEach(student => {
     
            
            if(student.name){
                count++
            }
        })
        this.students = count;

        return this;
    }
*/

//3. Add a new class method to class Section called count Passed Students
    /* 
        a. This method will count the number of students who passed.
        b. This method will update the passedStudents property
        c. This method is chainable and returns this.
    */   
//Answer:
      /*   countPassedStudents(){
            let count = 0;
           
            this.students.forEach(student => {
    
                student.willPass();
               
                if(student.isPassed){
                    count++
                }
            })
        }; */
//4. Add a new class method to class Section called computeSectionAve
    /* 
        a. This method will compute for the overall average grade of the class
        b. This method will update the sectionAve property
        c. This method is chainable and returns this.
    */

//Answer: 
/* computeSectionAve(){
        let count = 0;
       
        this.students.forEach(student =>{
        let avg = student.computeAve().average
        let sum = avg + count
        count = sum
        console.log(sum)
        })
        
        this.sectionAve = Math.round(count/5);

        return this;
    }; 
*/

        
   
